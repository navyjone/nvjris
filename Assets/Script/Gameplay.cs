﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Gameplay : MonoBehaviour {
    public GameObject TTetris;
    public GameObject ITetris;
    public GameObject OTetris;
    public GameObject ZTetris;
    public GameObject STetris;
    public GameObject LTetris;
    public GameObject LITetris;
    // private GameObject spawnPoint;
    public float MovingSpeed;
    public float RotateSpeed;
    public float EndTimeDelay;
    private bool IsStartClick;
    private GameObject activeTetris;
    private Board board;
    private NVJTimer timer;
    private bool IsRotateLocked;

    // Start is called before the first frame update
    void Start () {
        Initial();
    }

    // Update is called once per frame
    void Update () {
        if (IsStartClick) {
            if (board.SpawnPoint != null && this.activeTetris == null) {
                InitailizeActiveBlocks ();
                this.IsRotateLocked = false;
            }

            if (this.activeTetris != null) {
                var tetris = this.activeTetris.GetComponent<Tetris>();

                // Rotate
                if (IsRotating())
                {
                    tetris.Rotate(RotateSpeed);
                    if (!IsRotating())
                    {
                        tetris.NextRotate = Quaternion.identity;
                        if (!IsMovingDown())
                        {
                            tetris.IsMoveBack = true;
                        }
                    }
                }

                // Movement
                if (IsMovingDown ()) 
                {
                    tetris.MoveDown(this.MovingSpeed);
                    if (!IsMovingDown ()) tetris.UpdateMoveDownPosition ();
                } 
                else if (IsMovingBack())
                {
                    tetris.MoveUp(this.MovingSpeed);
                }
                else if (board.CanMoveDown (this.activeTetris.transform, tetris.Blocks)) 
                {
                    tetris.PreviousTransform = this.activeTetris.transform;
                    tetris.NextTransform = board.NextMove (this.activeTetris.transform);
                    tetris.IsMoveBack = false;
                } 
                else 
                {
                    // done move down
                    tetris.PreviousTransform = null;
                    tetris.NextTransform = null;
                    tetris.IsMoveBack = false;

                    if (this.timer.IsTimeUp ()) 
                    {
                        this.IsRotateLocked = true;
                        board.SetReserved (tetris.Blocks);
                        Destroy (this.activeTetris);
                        this.activeTetris = null;
                        this.timer.Reset();
                    } 
                    else
                    {
                        if (!this.timer.IsCounting()) this.timer.StartTimer(this.EndTimeDelay);                        
                    }
                }
            }
        }
    }

    public void Initial()
    {
        board = this.GetComponent<Board>();
        timer = this.GetComponent<NVJTimer>();
        board.Destroy();
        board.Initial();
        timer.Stop();
        Destroy(activeTetris);
        this.activeTetris = null;
    }

    public void GameStart () 
    {
        this.IsStartClick = true;
    }

    public void Restart()
    {
        board.Initial();
    }

    public void RotateLeftClick () {
        if (this.activeTetris == null) return;
        var tetris = this.activeTetris.GetComponent<Tetris>();
        var nextLeft = tetris.RotateLeft ();
        if (board.CanRotateLeft (nextLeft) && !this.IsRotateLocked) {
            tetris.UpdateRotateLeft (nextLeft);
        }
    }

    public void RotateRightClick () {
        if (this.activeTetris == null) return;
        var tetris = this.activeTetris.GetComponent<Tetris>();
        var nextRight = tetris.RotateRight ();
        if (board.CanRotateRight (nextRight) && !this.IsRotateLocked) {
            tetris.UpdateRotateRight (nextRight);
        }
    }

    private void InitailizeActiveBlocks () {
        this.activeTetris = Instantiate (RandomTetris (), board.SpawnPoint.transform.position, Quaternion.identity);
        this.activeTetris.SetActive (true);
        this.activeTetris.name = "activeBlock";
        this.activeTetris.GetComponent<Tetris>().SetBlocksToSpwan (board.SpawnPoint);
    }

    private GameObject RandomTetris () {
        var random = new System.Random ();
        var randomTetris = random.Next ((int) Tetris.Type.TType, (int) Tetris.Type.TType);
        switch (randomTetris) {
            case (int) Tetris.Type.TType:
                return this.TTetris;
            case (int) Tetris.Type.IType:
                return this.ITetris;
            case (int) Tetris.Type.OType:
                return this.OTetris;
            case (int) Tetris.Type.ZType:
                return this.ZTetris;
            case (int) Tetris.Type.SType:
                return this.STetris;
            case (int) Tetris.Type.LType:
                return this.LTetris;
            case (int) Tetris.Type.LIType:
                return this.LITetris;
            default:
                return this.ITetris;
        }
    }

    private bool IsMovingDown () {
        if (this.activeTetris == null) return false;
        var tetris = this.activeTetris.GetComponent<Tetris>();
        if (tetris.NextTransform == null) return false;
        return this.activeTetris.transform.position.y > tetris.NextTransform.transform.position.y;
    }

    private bool IsMovingBack()
    {
        if (this.activeTetris == null) return false;
        var tetris = this.activeTetris.GetComponent<Tetris>();
        if (tetris.PreviousTransform == null) return false;
        return (this.activeTetris.transform.position.y < tetris.PreviousTransform.transform.position.y) && tetris.IsMoveBack;
    }

    private bool IsRotating () {
        if (this.activeTetris == null) return false;
        var tetris = this.activeTetris.GetComponent<Tetris>();
        if (tetris.NextRotate == Quaternion.identity) return false;
        return this.activeTetris.transform.rotation != tetris.NextRotate;
    }
}