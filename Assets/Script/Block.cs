using UnityEngine;

public class Block : MonoBehaviour {
    public int XAxis;
    public int YAxis;
    public bool IsReserved;
    public string Color;
    public Transform BlockRef;
}