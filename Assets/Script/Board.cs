using System.Collections.Generic;
using UnityEngine;
public class Board : MonoBehaviour {
    public GameObject BlockReference;
    public int XAxisLength;
    public int YAxisLength;
    public int BoardHeight;
    public int SpawnAtXAxis;

    public GameObject SpawnPoint;
    public GameObject[, ] blocks;
    public bool IsRotateLocked;

    public void Initial()
    {
        var blockSize = BoardHeight / YAxisLength;
        for(var i = 0; i < this.transform.childCount; i++)
        {
            var reservedBlock = this.transform.GetChild(i);
            Destroy(reservedBlock.gameObject);
        }

        blocks = new GameObject[XAxisLength, YAxisLength];
        for (var y = 0; y < YAxisLength; y++)
        {
            for (var x = 0; x < XAxisLength; x++)
            {
                blocks[x, y] = Instantiate(BlockReference, GetCenterPosition(x, y, blockSize), Quaternion.identity);
                blocks[x, y].SetActive(true);
                blocks[x, y].name = $"block{x}-{y}";
                blocks[x, y].GetComponent<Block>().XAxis = x;
                blocks[x, y].GetComponent<Block>().YAxis = y;
            }
        }

        if (this.SpawnPoint == null)
        {
            this.SpawnPoint = Instantiate(BlockReference, GetCenterPosition(SpawnAtXAxis, -1, blockSize), Quaternion.identity);
            this.SpawnPoint.name = $"spawnPoint";
            this.SpawnPoint.SetActive(true);
            this.SpawnPoint.GetComponent<Block>().XAxis = SpawnAtXAxis;
            this.SpawnPoint.GetComponent<Block>().YAxis = -1;
        }
    }

    public bool CanMoveDown (Transform activeTetris, List<Transform> activeBlocks) {
        if (activeTetris.position == this.SpawnPoint.transform.position) return true;
        var canMoveDown = true;
        foreach (var block in activeBlocks) {
            var xAxis = block.GetComponent<Block> ().XAxis;
            var yAxis = block.GetComponent<Block> ().YAxis;
            var isBlockOnBoard = yAxis + 1 < this.blocks.GetLength (1);
            canMoveDown &= isBlockOnBoard && !this.blocks[xAxis, yAxis + 1].GetComponent<Block> ().IsReserved;
        }

        return canMoveDown;
    }

    public Transform NextMove (Transform activeTetris) {
        var centerBlock = activeTetris.GetChild (0);
        var xAxis = centerBlock.GetComponent<Block> ().XAxis;
        var yAxis = centerBlock.GetComponent<Block> ().YAxis;

        return this.blocks[xAxis, yAxis + 1].transform;
    }

    public void SetReserved (List<Transform> activeBlocks) {
        foreach (var block in activeBlocks) {
            var xAxis = block.GetComponent<Block> ().XAxis;
            var yAxis = block.GetComponent<Block> ().YAxis;
            block.gameObject.name = $"reservedBlock{xAxis}-{yAxis}";
            this.blocks[xAxis, yAxis].GetComponent<Block> ().BlockRef = block;
            this.blocks[xAxis, yAxis].GetComponent<Block> ().IsReserved = true;
            block.parent = this.transform;
        }
    }

    public bool CanRotateLeft (List<Transform> blocks) {
        var canRotateLeft = true;
        foreach (var block in blocks) {
            var xAxis = block.GetComponent<Block> ().XAxis;
            var yAxis = block.GetComponent<Block> ().YAxis;
            var isBlockOnBoard = (xAxis >= 0 && xAxis < this.blocks.GetLength (0)) &&
                            (yAxis >= 0 && yAxis < this.blocks.GetLength (1));
            canRotateLeft &= isBlockOnBoard && !this.blocks[xAxis, yAxis].GetComponent<Block> ().IsReserved;
        }

        return canRotateLeft;
    }

    public void Destroy()
    {
        if (blocks == null) return;
        foreach(var gameObject in blocks)
        {
            Destroy(gameObject);
        }
    }

    public bool CanRotateRight (List<Transform> blocks) {
        var canRotateRight = true;
        foreach (var block in blocks) {
            var xAxis = block.GetComponent<Block> ().XAxis;
            var yAxis = block.GetComponent<Block> ().YAxis;
            var isBlockOnBoard = (xAxis >= 0 && xAxis < this.blocks.GetLength (0)) &&
                (yAxis >= 0 && yAxis < this.blocks.GetLength (1));
            canRotateRight &= isBlockOnBoard && !this.blocks[xAxis, yAxis].GetComponent<Block> ().IsReserved;
        }

        return canRotateRight;
    }

    private Vector3 GetCenterPosition (int x, int y, int blockSize) {
        return new Vector3 ((x * blockSize) + (blockSize / 2), -((y * blockSize) + (blockSize / 2)), 0);
    }
}