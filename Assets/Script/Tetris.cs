﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tetris : MonoBehaviour {
    public enum Type {
        TType = 0,
        IType = 1,
        OType = 2,
        ZType = 3,
        SType = 4,
        LType = 5,
        LIType = 6,
    }
    public Type _Type;
    public Quaternion NextRotate = Quaternion.identity;
    public Transform NextTransform;
    public Transform PreviousTransform;
    public List<Transform> Blocks;
    public bool IsMoveBack = false;

    private Transform CenterBlock;

    private void Start () {

    }

    public void SetBlocksToSpwan (GameObject spawnPoint) {
        this.Blocks = new List<Transform> ();
        for (var i = 0; i < this.transform.childCount; i++) {
            var block = this.transform.GetChild (i);
            block.GetComponent<Block> ().XAxis += spawnPoint.GetComponent<Block> ().XAxis;
            block.GetComponent<Block> ().YAxis += spawnPoint.GetComponent<Block> ().YAxis;
            this.Blocks.Add (block);
            if (block.name == "CenterBlock") this.CenterBlock = block;
        }
    }

    public void MoveUp(float movingSpeed)
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, this.PreviousTransform.transform.position,
            movingSpeed * Time.deltaTime);
    }

    public void MoveDown (float movingSpeed) 
    {
        this.transform.position = Vector3.MoveTowards (this.transform.position, this.NextTransform.transform.position,
            movingSpeed * Time.deltaTime);
    }

    public void UpdateMoveDownPosition () {
        // this.transform.position = nextMove.transform.position;
        foreach (var block in this.Blocks) {
            block.GetComponent<Block> ().YAxis += 1;
        }
    }

    public void Rotate (float rotateSpeed) {
        // this.activeTetris.transform.Rotate (GetRotateDirection (), RotateSpeed * Time.deltaTime);
        this.transform.rotation = Quaternion.Slerp (this.transform.rotation, this.NextRotate, rotateSpeed * Time.fixedDeltaTime);
    }

    public List<Transform> RotateLeft () {
        var centerXAxis = this.CenterBlock.GetComponent<Block> ().XAxis;
        var centerYAxis = this.CenterBlock.GetComponent<Block> ().YAxis;
        var nextLeft = new List<Transform> ();
        foreach (var block in this.Blocks) {
            int xAxisNext;
            int yAxisNext;
            var xAxis = block.GetComponent<Block> ().XAxis;
            var yAxis = block.GetComponent<Block> ().YAxis;
            if (block.name == "CenterBlock") {
                nextLeft.Add (block);
                continue;
            }
            switch (this._Type) {
                case Tetris.Type.TType:
                    if (xAxis - centerXAxis != 0) xAxisNext = centerXAxis;
                    else if (yAxis > centerYAxis) xAxisNext = centerXAxis + 1;
                    else xAxisNext = centerXAxis - 1;

                    if (yAxis != centerYAxis) yAxisNext = centerYAxis;
                    else if (xAxis > xAxisNext) yAxisNext = centerYAxis - 1;
                    else yAxisNext = centerYAxis + 1;

                    block.GetComponent<Block> ().XAxis = xAxisNext;
                    block.GetComponent<Block> ().YAxis = yAxisNext;
                    nextLeft.Add (block);
                    break;
                case Tetris.Type.IType:

                    break;
                case Tetris.Type.OType:

                    break;
                case Tetris.Type.ZType:

                    break;
                case Tetris.Type.SType:

                    break;
                case Tetris.Type.LType:

                    break;
                case Tetris.Type.LIType:

                    break;
            }
        }

        return nextLeft;
    }

    public List<Transform> RotateRight () {
        var centerXAxis = this.CenterBlock.GetComponent<Block> ().XAxis;
        var centerYAxis = this.CenterBlock.GetComponent<Block> ().YAxis;
        var nextRight = new List<Transform> ();
        foreach (var block in this.Blocks) {
            int xAxisNext;
            int yAxisNext;
            var xAxis = block.GetComponent<Block> ().XAxis;
            var yAxis = block.GetComponent<Block> ().YAxis;
            if (block.name == "CenterBlock") {
                nextRight.Add (block);
                continue;
            }
            switch (this._Type) {
                case Tetris.Type.TType:
                    if (xAxis - centerXAxis != 0) xAxisNext = centerXAxis;
                    else if (yAxis > centerYAxis) xAxisNext = centerXAxis - 1;
                    else xAxisNext = centerXAxis + 1;

                    if (yAxis != centerYAxis) yAxisNext = centerYAxis;
                    else if (xAxis > xAxisNext) yAxisNext = centerYAxis + 1;
                    else yAxisNext = centerYAxis - 1;

                    block.GetComponent<Block> ().XAxis = xAxisNext;
                    block.GetComponent<Block> ().YAxis = yAxisNext;
                    nextRight.Add (block);
                    break;
                case Tetris.Type.IType:

                    break;
                case Tetris.Type.OType:

                    break;
                case Tetris.Type.ZType:

                    break;
                case Tetris.Type.SType:

                    break;
                case Tetris.Type.LType:

                    break;
                case Tetris.Type.LIType:

                    break;
            }
        }

        return nextRight;
    }

    public void UpdateRotateLeft (List<Transform> nextLeft) {
        this.Blocks = nextLeft;
        this.NextRotate = Quaternion.Euler (this.NextRotate.eulerAngles + (Vector3.forward * 90));
    }

    public void UpdateRotateRight (List<Transform> nextRight) {
        this.Blocks = nextRight;
        this.NextRotate = Quaternion.Euler (this.NextRotate.eulerAngles + (Vector3.back * 90));
    }

}