using UnityEngine;

public class NVJTimer : MonoBehaviour
{
    private float timer;
    private float timeCount;
    private bool isCounting;
    private bool isTimeUp;

    private void Update()
    {
        if (isCounting)
        {
            if (timeCount > 0) timeCount -= Time.deltaTime;
            else
            {
                isCounting = false;
                isTimeUp = true;
            }
        }
    }

    private void Initial()
    {
        this.timeCount = this.timer;
        this.isCounting = true;
        this.isTimeUp = false;
    }

    public void StartTimer(float timeCount)
    {
        this.timer = timeCount;
        Initial();
    }

    public void Stop()
    {
        this.isCounting = false;
    }

    public void Resume()
    {
        this.isCounting = true;
    }

    public void Restart()
    {
        this.Initial();
    }

    public void Reset()
    {
        isCounting = false;
        isTimeUp = false;
    }

    public bool IsCounting()
    {
        return this.isCounting;
    }

    public bool IsTimeUp()
    {
        return this.isTimeUp;
    }
}